#! /bin/bash

#
# Script for customizing Fedora workstation (with gnome) the way I want
# Email:    spam[at]deadrat[dot]in
#
# Rough draft 
# work in progress
#
#

update_dnf()
{
read -p "Do you want to update DNF ? : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf update -y dnf
    dnf update
else
    printf "DNF not updated \n\n"
fi
}


update_existing()
{

read -p "Do you want to update existing packages? : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf update -y dnf
    dnf update
else
    printf "Packages not updated \n\n"
fi
}


enable_rpmfusion()
{
read -p "Do you want to enable RPM fusion free : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf install -y --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
else
    printf "RPM Fusion Free Not enabled \n\n"
fi

read -p "Do you want to enable RPM fusion NON free : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf install -y --nogpgcheck http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
else
    printf "RPM Fusion NON Free Not enabled \n\n"
fi
}



install_other_packs()
{
read -p "Do you want to install  common free packages ?  : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf install smplayer gnome-tweak-tool gimp swell-foop four-in-a-row gtg inkscape nautilus-open-terminal bluez-hid2hci bluez-hid2hci emacs screen vim icecat qbittorrent hexchat putty youtube-dl gnome-power-manager xbacklight screen gnome-nettool gnome-shell-browser-plugin
else
    printf "other packs Not installed. \n\n"
fi
}


install_other_nonfree_packs()
{
read -p "Do you want to install some common NON-free packs (including multimedia plugins)?  : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf install gstreamer-ffmpeg gstreamer-plugins-bad gstreamer-plugins-bad-free-extras gstreamer-plugins-bad-nonfree gstreamer-plugins-ugly x264 vlc

else
    printf "other nonfree packs Not installed. \n\n"
fi
}



install_googlechrome()
{
read -p "Do you want to REALLY install Google Chrome?  : (y/n)" response
if [[ $response = y || $response = Y ]];
then
    dnf install -y  https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm         #--nogpgcheck #add before url
    dnf install fedora-user-agent-chrome.noarch
else
    printf "Google Chrome not installed"
fi
}


gnome_ext_enabler()
{
#get and enable extension 

printf "Installing Extension Via Bash is Pain in the Arse, so do it manually until someone adds this functionality \n\n"
#wget -O /tmp/extension.tgz "https://github.com/dmo60/CoverflowAltTab/archive/master.zip"
#mkdir -p "$HOME/.local/share/gnome-shell/extensions/xxxx@xxx"
#unzip /tmp/extension.zip -d "$HOME/.local/share/gnome-shell/extensions/xxxxxxxxx@xxx"
#enabled-extensions=['user-theme@gnome-shell-extensions.gcampax.github.com', 'Bottom_Panel@rmy.pobox.com', 'alternate-tab@gnome-shell-extensions.gcampax.github.com', 'lockkeys@vaina.lt', 'RecentItems@bananenfisch.net', 'ShutdownTimer@neumann', 'clipboard-indicator@tudmotu.com', 'turnoffdisplay@simonthechipmunk.noreply.com', 'CoverflowAltTab@palatis.blogspot.com', 'uptime-indicator@gniourfgniourf.gmail.com']
}

gsettings_conf()
{
#org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type 'suspend'
#org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 900
#org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type 'suspend'
#org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 1800
#gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'interactive'


#########
#gsettings set org.gnome.settings-daemon.plugins.power power-button-action 'interactive'
gsettings set org.gnome.gedit.preferences.editor scheme 'cobalt'
gsettings set org.gnome.gedit.preferences.editor create-backup-copy true
gsettings set org.gnome.gedit.preferences.editor auto-save true
gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
gsettings set org.gnome.gedit.preferences.editor tabs-size uint32 4
gsettings set org.gnome.gedit.preferences.editor insert-spaces true
}

custom_shortcuts()
{
printf "do it manually \n\n"
#[org.gnome.nautilus.preferences]
#sort-directories-first=true

#[org.gnome.shell.overrides]
#button-layout=':maximize,close'

#[org.gnome.desktop.wm.keybindings]
#show-desktop=['<Super>D']
#gnome-system-monitor=['<Ctrl><Shift><Esc>']
#gnome-terminal=['<Super>R']
}





## Main
update_dnf
update_existing
enable_rpmfusion
install_googlechrome
install_other_packs
install_other_nonfree_packs
gnome_ext_enabler
custom_shortcuts
#gsettings_conf
