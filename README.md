Ambuttu.sh
====
Shell script for customizing fedora
----
#### A simple shell script to customize the Fedora to make it work the way I want.

This should do the customizations in recent Fedora releases. 

This installs the update, adds RPM fusion free and non-free repos, installs media plugins and players. It doesn't customize gnome desktop environment yet.

